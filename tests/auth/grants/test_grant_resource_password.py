import config
import json
import unittest

from flask import current_app
from micro_auth_authn import create_app, db
from micro_auth_authn.models.clients import Client
from micro_auth_authn.models.roles import Role
from micro_auth_authn.models.scopes import Scopes
from sqlalchemy.exc import SQLAlchemyError


class TestGrantResourcePassword(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        admin_scopes = [
            Scopes.CREATE_STOCK.value,
            Scopes.READ_STOCK.value,
            Scopes.UPDATE_STOCK.value,
            Scopes.DELETE_STOCK.value,
        ]
        user_scopes = [
            Scopes.READ_STOCK.value,
        ]

        cls.app = create_app(config.TestConfig)
        with cls.app.app_context():
            db.drop_all()
            db.create_all()
            db.session.add(Client(client_id='', client_secret=''))
            db.session.add(Role(name='admin', scopes=json.dumps(admin_scopes)))
            db.session.add(Role(name='user', scopes=json.dumps(user_scopes)))
            try:
                db.session.commit()
            except SQLAlchemyError as e:
                print('Cannot initialize db.\n')

            cls.test_app = current_app.test_client()

    @classmethod
    def tearDownClass(cls):
        with cls.app.app_context():
            db.drop_all()
            db.session.remove()
