import config
import json
import unittest

from flask import current_app
from micro_auth_authn import create_app, db
from micro_auth_authn.models.roles import Role
from micro_auth_authn.models.scopes import Scopes
from sqlalchemy.exc import SQLAlchemyError


class TestApp(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.app = create_app(config.TestConfig)
        with cls.app.app_context():
            db.drop_all()
            db.create_all()
            try:
                db.session.commit()
            except SQLAlchemyError as e:
                print('Cannot initialize db.\n')

            cls.test_app = current_app.test_client()

    @classmethod
    def tearDownClass(cls):
        with cls.app.app_context():
            db.drop_all()
            db.session.remove()

    def test_invalid_url(self):
        response = self.test_app.get('/something')
        print('response status code: ' + str(response.status_code))
        self.assertEqual(response.status_code, 404)

    def test_user_login_with_username(self):
        #self.test_register_user()

        response = self.test_app.post(
            '/authn/users/login',
            data=dict(
                loginname='test_username',
                password='testPassword'
            )
        )

        print('response data: ' + str(response.data, 'utf-8'))
        self.assertEqual(response.status_code, 200)

    def test_user_login_with_email(self):
        #self.test_register_user()

        response = self.test_app.post(
            '/authn/users/login',
            data=dict(
                loginname='test.username@testmail.com',
                password='testPassword'
            )
        )

        print('response data: ' + str(response.data, 'utf-8'))
        self.assertEqual(response.status_code, 200)
