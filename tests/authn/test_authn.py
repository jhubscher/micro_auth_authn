import config
import json
import unittest

from flask import current_app
from micro_auth_authn import create_app, db
from micro_auth_authn.models.roles import Role
from micro_auth_authn.models.scopes import Scopes
from sqlalchemy.exc import SQLAlchemyError


class TestApp(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        admin_scopes = [
            Scopes.READ_CERT.value,
            Scopes.READ_PUBLIC_KEY.value,
            Scopes.WRITE_CERT.value,
            Scopes.WRITE_PUBLIC_KEY.value,
        ]
        user_scopes = [
            Scopes.READ_CERT.value,
            Scopes.READ_PUBLIC_KEY.value,
        ]

        cls.app = create_app(config.TestConfig)
        with cls.app.app_context():
            db.drop_all()
            db.create_all()
            db.session.add(Role(name='admin', scopes=json.dumps(admin_scopes)))
            db.session.add(Role(name='user', scopes=json.dumps(user_scopes)))
            try:
                db.session.commit()
            except SQLAlchemyError as e:
                print('Cannot initialize db.\n')

            cls.test_app = current_app.test_client()

    @classmethod
    def tearDownClass(cls):
        with cls.app.app_context():
            db.drop_all()
            db.session.remove()

    def test_register_user(self):
        response = self.test_app.post(
            '/authn/users/register',
            data=dict(
                username='test_username',
                email='test.username@testmail.com',
                password='testPassword'
            )
        )

        print('response data: ' + str(response.data, 'utf-8'))
        self.assertEqual(response.status_code, 200)
    

    def test_user_login_with_username(self):
        self.test_register_user()

        response = self.test_app.post(
            '/authn/users/login',
            data=dict(
                loginname='test_username',
                password='testPassword'
            )
        )

        print('response data: ' + str(response.data, 'utf-8'))
        self.assertEqual(response.status_code, 200)

    def test_user_login_with_email(self):
        self.test_register_user()

        response = self.test_app.post(
            '/authn/users/login',
            data=dict(
                loginname='test.username@testmail.com',
                password='testPassword'
            )
        )

        print('response data: ' + str(response.data, 'utf-8'))
        self.assertEqual(response.status_code, 200)
