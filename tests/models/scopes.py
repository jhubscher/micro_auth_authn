import config
import json
import unittest

from flask import current_app
from micro_auth_authn import create_app, db
from micro_auth_authn.models.roles import Role
from micro_auth_authn.models.scopes import Scopes
from sqlalchemy.exc import SQLAlchemyError


class TestApp(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.app = create_app(config.TestConfig)
        with cls.app.app_context():
            db.drop_all()
            db.create_all()
            try:
                db.session.commit()
            except SQLAlchemyError as e:
                print('Cannot initialize db.\n')

            cls.test_app = current_app.test_client()

    @classmethod
    def tearDownClass(cls):
        with cls.app.app_context():
            db.drop_all()
            db.session.remove()

    def test_create_scope(self):
       pass
