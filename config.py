class Config(object):
    SQLALCHEMY_DATABASE_URI = r'sqlite:///micro_auth_authn.sqlite'


class DevConfig(Config):
    SECRET_KEY = 'dev'
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = r'sqlite:///../dev_micro_auth_authn.sqlite'


class ProdConfig(Config):
    SECRET_KEY = 'prod'
    SQLALCHEMY_DATABASE_URI = r'sqlite:///../prod_micro_auth_authn.sqlite'


class TestConfig(Config):
    SECRET_KEY = 'test'
    TESTING = True
    SQLALCHEMY_DATABASE_URI = r'sqlite:///../test_micro_auth_authn.sqlite'
