from micro_auth_authn import db


class Client(db.Model):
    __tablename__ = 'clients'

    id = db.Column(db.Integer(), primary_key=True)
    client_id = db.Column(db.LargeBinary(), nullable=False, unique=True)
    client_secret = db.Column(db.LargeBinary(), nullable=False, unique=True)

    def __init__(self, client_id, client_secret):
        self.client_id = client_id
        self.client_secret = client_secret
