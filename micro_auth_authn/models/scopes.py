from enum import Enum
from micro_auth_authn import db


class Scope(db.Model):
    __tablename__ = 'scopes'

    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(255), nullable=False, unique=True)
    access_token_id = db.Column(db.Integer(), db.ForeignKey('access_tokens.id'))

    def __init__(self, name):
        self.name = name

class StockScopes(Enum):
    CREATE_STOCK = 'stock.create'
    READ_STOCK = 'stock.read'
    UPDATE_STOCK = 'stock.update'
    DELETE_STOCK = 'stock.delete'

class CartScopes(Enum):
    CREATE_STOCK = 'cart.create'
    READ_STOCK = 'cart.read'
    UPDATE_STOCK = 'cart.update'
    DELETE_STOCK = 'cart.delete'