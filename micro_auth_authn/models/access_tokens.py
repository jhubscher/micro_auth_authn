from micro_auth_authn import db


class AccessToken(db.Model):
    __tablename__ = 'access_tokens'

    id = db.Column(db.Integer(), primary_key=True)
    token = db.Column(db.LargeBinary(), nullable=False, unique=True)
    refresh_token = db.Column(db.LargeBinary(), unique=True)
    auth_code = db.Column(db.Boolean(), nullable=False)
    user_id = db.Column(db.Integer(), db.ForeignKey('users.id'), nullable=False)
    issued_date = db.Column(db.DateTime(), nullable=False)
    expiry_date = db.Column(db.DateTime(), nullable=False)
    scopes = db.relationship(
       'Scope',
       backref='access_tokens',
       lazy='dynamic'
    )

    def __init__(self, token, refresh_token, auth_code, user_id, issued_date, expiry_date, scopes):
        self.token = token
        self.refresh_token = refresh_token
        self.auth_code = auth_code
        self.user_id = user_id
        self.issued_date = issued_date
        self.expiry_date = expiry_date
        self.scopes = scopes
