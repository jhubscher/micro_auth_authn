from micro_auth_authn import db


class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer(), primary_key=True)
    #client_id = db.Column(db.LargeBinary(), db.ForeignKey('clients.id'), nullable=False, unique=True)
    username = db.Column(db.String(255), nullable=False, unique=True)
    email = db.Column(db.String(255), nullable=False, unique=True)
    password = db.Column(db.LargeBinary(), nullable=False)
    salt = db.Column(db.LargeBinary(), nullable=False)
    user_type = db.Column(db.String, nullable=False)

    def __init__(self, username, email, password, salt, user_type):
        #self.client_id = client_id
        self.username = username
        self.email = email
        self.password = password
        self.salt = salt
        self.user_type = user_type
