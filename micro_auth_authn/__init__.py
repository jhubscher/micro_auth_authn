import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()


def create_app(config):
    from micro_auth_authn.auth import bp as auth_bp
    from micro_auth_authn.auth.grants.resource_password import bp as auth_grant_resource_password_bp
    from micro_auth_authn.authn.authn import bp as authn_bp

    global db

    app = Flask(__name__, instance_relative_config=True)
    app.register_blueprint(auth_bp)
    app.register_blueprint(auth_grant_resource_password_bp)
    app.register_blueprint(authn_bp)

    db.init_app(app)

    if config is None:
        raise Exception(
            'ConfigurationException: instantiated application '
            'without configuration object.'
        )
    else:
        app.config.from_object(config)

    # Ensure the instance folder exists.
    try:
        os.makedirs(app.instance_path)
    except OSError as e:
        pass

    @app.route('/hello')
    def hello():
        return 'Hello, OAuth!'

    return app
