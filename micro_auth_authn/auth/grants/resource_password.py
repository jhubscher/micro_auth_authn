import datetime
import json
import jwt
import os
import time

from cryptography.exceptions import InvalidKey
from flask import Blueprint, jsonify, request, make_response
from micro_auth_authn import db
from micro_auth_authn.auth import get_admin_scopes, get_user_scopes
from micro_auth_authn.authn import hash_password, user_exists, verify_password
from micro_auth_authn.models.access_tokens import AccessToken
from micro_auth_authn.models.clients import Client
from micro_auth_authn.models.scopes import Scope
from micro_auth_authn.models.users import User
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.sql.expression import or_


bp = Blueprint(
        'auth_grant_resource_password',
        __name__,
        url_prefix='/auth/grants/resource_password'
    )


@bp.route('/login', methods=['POST'])
def login():
    loginname = request.form['loginname']
    user = User.query.filter(
        or_(User.username == loginname, User.email == loginname)
    ).first()

    #client = Client.query.filter_by(client_id=user.client_id).first()
    salt = user.salt
    password_received = request.form['password']
    password_hash = user.password

    try:
        verify_password(password_received, password_hash, salt)
    except InvalidKey as e:
        return make_response(
            jsonify(dict(msg='Invalid username/email & password combo.')),
            401
        )

    iat = int(round(time.time()))
    exp = iat + datetime.timedelta(minutes=60).seconds()

    payload = dict(
        exp=exp,
        iss=os.environ['ISSUER'],
        iat=iat,
        aud=os.environ['AUDIENCE'],
        username=user.username,
        email=user.email,
    )

    encoded_token = jwt.encode(
        payload,
        'secret',
        #client.client_secret,
        algorithm='HS256'
    )

    scopes = []
    # get_user_scopes if
    if user.user_type is 'user':
        scopes = get_user_scopes()
    elif user.user_type is 'admin':
        scopes = get_admin_scopes()

    new_access_token = AccessToken(
        token=encoded_token,
        refresh_token=None,
        auth_code=False,
        user_id=user.id,
        issued_date=iat,
        expiry_date=exp,
        scopes=scopes
    )

    db.session.add(new_access_token)
    try:
        db.session.commit()
    except SQLAlchemyError as e:
        return make_response(jsonify(dict(msg='Cannot create JWT.')), 500)

    return make_response(
        jsonify(
            dict(
                access_token=str(encoded_token, encoding='utf-8'),
                scope='',
                type='Bearer',
                expires_in='60000'
            )
        ),
        200
    )
