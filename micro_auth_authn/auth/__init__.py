import datetime
import jwt
import os

from flask import Blueprint, jsonify, request, make_response
from micro_auth_authn import db
from micro_auth_authn.models.access_tokens import AccessToken
from micro_auth_authn.models.clients import Client
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.sql.expression import or_


bp = Blueprint('auth', __name__, url_prefix='/auth')


@bp.route('/validate_access_token', methods=['POST'])
def validate_access_token():
    token_header = request.headers['Authorization']
    
    if 'Bearer' in token_header:
        token = token_header.split(sep=' ')[1]

        if token is not None:
            print('TOKEN is: ' + token)

def get_user_scopes():
    pass


def get_admin_scopes():
    pass