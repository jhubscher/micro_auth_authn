from cryptography.hazmat.primitives.kdf.scrypt import Scrypt
from cryptography.hazmat.backends import default_backend
from micro_auth_authn.models.users import User
from sqlalchemy.sql.expression import or_


def get_scrypt_kdf(salt, length=32, n=2**14, r=8, p=1):
    backend = default_backend()

    return Scrypt(
        salt=salt,
        length=length,
        n=n,
        r=r,
        p=p,
        backend=backend
    )


def hash_password(password, salt):
    kdf = get_scrypt_kdf(salt=salt)
    return kdf.derive(bytes(password, 'utf-8'))


def verify_password(password, password_hash, salt):
    kdf = get_scrypt_kdf(salt=salt)
    kdf.verify(bytes(password, 'utf-8'), password_hash)


def user_exists(user):
    return len(
        User.query.filter(
            or_(
                User.email == user.email,
                User.username == user.username
            )
        ).all()
    ) > 0
