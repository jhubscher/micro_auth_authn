import datetime
import json

import jwt
import os
import uuid

from cryptography.exceptions import InvalidKey
from flask import Blueprint, jsonify, request, make_response
from micro_auth_authn import db
from micro_auth_authn.authn import authn, hash_password, user_exists, verify_password
from micro_auth_authn.models.access_tokens import AccessToken
from micro_auth_authn.models.clients import Client
from micro_auth_authn.models.users import User
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.sql.expression import or_

bp = Blueprint('authn', __name__, url_prefix='/authn')


@bp.route('/users/register', methods=['POST'])
def register_user():
    username = request.form['username']
    email = request.form['email']
    password = request.form['password']
    salt = os.urandom(16)
    password_hash = hash_password(password, salt)

    client_id = uuid.uuid4()
    client_secret = uuid.uuid4()
    new_client = Client(client_id=client_id, client_secret=client_secret)

    new_user = User(
        client_id=client_id,
        username=username,
        email=email,
        password=password_hash,
        salt=salt
    )

    if user_exists(new_user):
        return make_response(
            jsonify(dict(msg='User with this username/email already exists.')),
            401
        )
    elif not username:
        return make_response(jsonify(dict(msg='Missing Username field.')), 401)
    elif not email:
        return make_response(jsonify(dict(msg='Missing Email field.')), 401)
    elif not password:
        return make_response(jsonify(dict(msg='Missing Password field.')), 401)
    else:
        db.session.add(new_client)
        db.session.add(new_user)
        try:
            db.session.commit()
        except SQLAlchemyError as e:
            return make_response(jsonify(dict(msg='Unable to register user.')), 500)

        return make_response(
            jsonify(
                dict(
                    msg='Successfully registered user.',
                    data=json.dumps(
                        dict(
                            username=username,
                            email=email,
                            client_id=client_id,
                            client_secret=client_secret
                        )
                    )
                )
            ),
            200
        )
