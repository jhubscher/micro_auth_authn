import config
import os
from micro_auth_authn import create_app, db


if __name__ == '__main__':
    os.environ['ISSUER'] = 'https://127.0.0.1:5000'
    os.environ['AUDIENCE'] = 'https://127.0.0.1:5000'

    app = create_app(config.DevConfig)
    with app.app_context():
        db.create_all()

    app.run(ssl_context='adhoc')
