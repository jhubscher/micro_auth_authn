from setuptools import find_packages, setup

with open("README.md", "r") as file_handler:
    long_description = file_handler.read()

setup(
    name="micro-auth-authn",
    url="https://github.com/jhubscher/micro_auth_authn",
    version="1.0.0",
    author="Jordan Hubscher",
    author_email="jordanhubscher@gmail.com",
    description="A small OAuth 2.0 & OpenID Connect provider microservice.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
)
